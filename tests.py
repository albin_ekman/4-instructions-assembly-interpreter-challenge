from solution import *

# Test mov int
code = '''\
mov a 5'''


def test_1():
    print(code.splitlines())
    assert evaluate(code.splitlines()) == {'a': 5}


# Test mov existing register
code2 = '''\
mov a 5
mov b a
'''


def test_2():
    assert evaluate(code2.splitlines()) == {'a': 5, 'b': 5}


# Test inc new register
code3 = '''\
inc a
'''


def test_3():
    assert evaluate(code3.splitlines()) == {'a': 1}


# Test inc existing register
code4 = '''\
inc a
inc a
'''


def test_4():
    assert evaluate(code4.splitlines()) == {'a': 2}


# Test dec new register
code5 = '''\
dec a
'''


def test_5():
    assert evaluate(code5.splitlines()) == {'a': -1}


# Test dec existing register
code6 = '''\
dec a
dec a
'''


def test_6():
    assert evaluate(code6.splitlines()) == {'a': -2}


code7 = '''\
mov a 5
inc a
dec a
dec a
jmp a -1
inc a'''


def test_7():
    assert evaluate(code7.splitlines()) == {'a': 1}


code8 = '''\
mov c 12
mov b 0
mov a 200
dec a
inc b
jmp a -2
dec c
mov a b
jmp c -5
jmp 0 1
mov c a'''


def test_8():
    assert evaluate(
        code8.splitlines()) == {
        'a': 409600,
        'c': 409600,
        'b': 409600}
